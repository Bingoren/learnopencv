#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;
int main( int argc, char** argv)
{
    /*************Part1 读取与显示图片****************/
    Mat tangWei = imread("/home/hri/Project/LearnOpenCV/task2_imagePprocessPreliminary/tangwei.jpg");// 读取图片
    namedWindow("中意",0); // 创建显示图片窗口的名字
    imshow("中意", tangWei);// 进行显示
    waitKey(0);// 0代表等待用户按键处罚程序继续运行


    /*************Part2 初级图像融合*****************/
    Mat img = imread("/home/hri/Project/LearnOpenCV/task2_imagePprocessPreliminary/Zed.jpeg");
    Mat logo = imread("/home/hri/Project/LearnOpenCV/task2_imagePprocessPreliminary/logo.jpeg");

    //先显示图片
    namedWindow("Zed");
    imshow("Zed",img);
    waitKey(0);

    namedWindow("Logo");
    imshow("Logo",logo);
    waitKey(0);
    //定义一个Mat的量来存放ROI
    Mat imageROI;

    imageROI = img(Rect(0, 800, logo.cols, logo.rows));// 函数参数介绍：图像左上角x坐标， 图像左上角y坐标， 宽度， 长度

    //将LOGO加到原图上去
    addWeighted(imageROI, 0.5, logo, 0.3, 0., imageROI);


    namedWindow("ZedWithLogo");
    imshow("ZedWithLogo", img);
    waitKey(0);

    /*************Part3 图像的输出******************/

    imwrite("WeEnjoyLoL byBingo.jpg", img);
    waitKey();

    return 0;


}