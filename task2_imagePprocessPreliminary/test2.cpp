//
// Created by hri on 18-11-5.
// 主要就是创建一个滑动条
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>


using namespace cv;
#define WINDOW_NAME "线性混合示例"


const int g_nMaxAlphaValue = 100;// Alpha值的最大值
int g_nAlphaValueSlider;//滑动条对应的变量
double g_dAlphaValue;
double g_dBeteValue;


// --申明一些存储图像的变量
Mat g_srcImage1;
Mat g_srcImage2;
Mat g_srcImage;


void on_Trackbar( int, void*)
{
    // --求出当前的alpha值对应的最大值的比例
    g_dAlphaValue = (double) g_nAlphaValueSlider / g_nMaxAlphaValue;
    // --beta的值就是用1减去alpha的值咯
    g_dBeteValue = (1.0 - g_dAlphaValue );


    // --根据alpha和deta的值进行线性混合
    addWeighted(g_srcImage1, g_dAlphaValue, g_srcImage2, g_dBeteValue, 0.0, g_srcImage);
    imshow(WINDOW_NAME, g_srcImage);
}

int main( int argc, char** argv)
{
    g_srcImage1 = imread("/home/hri/Project/LearnOpenCV/task2_imagePprocessPreliminary/0.jpeg");
    g_srcImage2 = imread("/home/hri/Project/LearnOpenCV/task2_imagePprocessPreliminary/Ayanami.jpeg");

    if ( !g_srcImage1.data )
    {
        printf("Error,No Image1!");
    }
    if ( !g_srcImage2.data)
    {
        printf("Error,No Image2!");
    }


    g_nAlphaValueSlider = 70;

    namedWindow(WINDOW_NAME,1);

    // --在创建的窗体中创建一个滑动条控件
    char TrackbarName[50];
    sprintf(TrackbarName, "透明度 %d", g_nMaxAlphaValue);

    createTrackbar(TrackbarName, WINDOW_NAME, &g_nAlphaValueSlider, g_nMaxAlphaValue, on_Trackbar);


    on_Trackbar( g_nAlphaValueSlider, 0);

    waitKey(0);
    return 0;

}



