#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>

using namespace std;
using namespace cv;

bool ROI_AddImage();
bool LinearBlending();
bool ROI_LinearBlending();


int main( )
{
    //system("color 5E");

    //if (ROI_AddImage())
    //if (LinearBlending())
    //if (ROI_LinearBlending())
    if (ROI_AddImage()&&LinearBlending()&&ROI_LinearBlending())
    {
        cout<<endl<<"well , the image you want";
    }
    waitKey(0);
    return 0;
}

bool ROI_AddImage()
{
    Mat srcImage1= imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/riwen.jpeg");
    Mat logoImage= imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/logo.jpeg");

    if(!srcImage1.data ) { printf("load error! \n"); return false; }
    if(!logoImage.data ) { printf("load error! \n"); return false; }

    Mat imageROI = srcImage1(Rect(0, 0, logoImage.cols,logoImage.rows));

    Mat mask = imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/logo.jpeg", 0);

    logoImage.copyTo(imageROI,mask);


    namedWindow("<1>利用ROI实现图像叠加实例窗口");
    imshow("<1>利用ROI实现图像叠加实例窗口",srcImage1);

    imwrite("EnjoyLoLByBingo.jpg", srcImage1);
    waitKey();

    return true;
}

bool LinearBlending()
{
    // --定义两个局部变量
    double alphaValue = 0.3;
    double betaValue;

    Mat srcImage2, srcImage3, dstImage;

    // --读取图像
    srcImage2 = imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/chuyin.jpeg");
    srcImage3 = imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/wanxia.jpg");

    if(!srcImage2.data ) { printf("load error! \n"); return false; }
    if(!srcImage3.data ) { printf("load error! \n"); return false; }

    // --进行图像混合加权操作
    betaValue = (1.0 - alphaValue);
    addWeighted( srcImage2, alphaValue, srcImage3, betaValue, 0.0, dstImage);

    // --显示图片
    namedWindow("<2>LinearBlendingWindow[original]", 1);
    imshow("<3>LinearBlendingWindow[fused]", srcImage2);

    namedWindow("<3>LinearBlendingWindow[fused]", 1);
    imshow("<3>LinearBlendingWindow[fused]",dstImage);

    imwrite("EnjoyChuyinByBingo.jpg", dstImage);
    waitKey();

    return true;
}


bool ROI_LinearBlending()
{
    // --image Reading
    Mat srcImage4 = imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/riwen.jpeg");
    Mat logoImage = imread("/home/hri/Project/LearnOpenCV/task3_imposedAndBlending/logo.jpeg");

    if(!srcImage4.data ) { printf("load error! \n"); return false; }
    if(!logoImage.data ) { printf("load error! \n"); return false; }


    // --Define a Mat and set a ROI area
    Mat imageROI;

    imageROI = srcImage4(Rect(0, 0, logoImage.cols, logoImage.rows));
    // --Method2
    //imageROI = srcImage4(Rect(200, 250+logoImage.rows),Range(200, 200+logoImage.cols));
    addWeighted(imageROI, 0.5, logoImage, 0.3, 0., imageROI);

    // --Display the image
    namedWindow("<4>ROI_LinearBlendingWindow");
    imshow("<4>ROI_LinearBlendingWindow",srcImage4);

    imwrite("ROI_LinearBlending_Bingo.jpg",srcImage4);

    return true;

}

